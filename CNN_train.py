import tensorflow as tf
import os
from tensorflow import keras
from tensorflow.keras import Sequential
from tensorflow.keras.layers import Dense,Conv2D,MaxPooling2D,Flatten,Dropout
from tensorflow.keras.callbacks import EarlyStopping
from tensorflow.keras.preprocessing.image import ImageDataGenerator
import pandas as pd
import matplotlib.pyplot as plt

base_dir = 'MNIST Dataset JPG format'
train_dir = os.path.join(base_dir,'MNIST - JPG - training')
test_dir = os.path.join(base_dir,'MNIST - JPG - testing')

callbacks = [
						EarlyStopping(patience=2)
]

training_set = tf.keras.preprocessing.image_dataset_from_directory(
train_dir,
seed=101,
color_mode="grayscale",
image_size=(28,28),
batch_size=32 )

validation_set = tf.keras.preprocessing.image_dataset_from_directory(
test_dir,
seed=101,
color_mode="grayscale",
image_size=(28,28),
batch_size=32)

data_augmentation = tf.keras.Sequential(
	[
		tf.keras.layers.experimental.preprocessing.RandomFlip("horizontal",
  																		input_shape=(28,28,1)),
		tf.keras.layers.experimental.preprocessing.RandomRotation(0.2),
		tf.keras.layers.experimental.preprocessing.RandomZoom(0.2),
	]
)

model = Sequential([
	data_augmentation,
	tf.keras.layers.experimental.preprocessing.Rescaling(1./255),
	Conv2D(filters=32, kernel_size=(3,3), activation='relu'),
	MaxPooling2D(pool_size=(2,2)),

	Conv2D(filters=32, kernel_size=(3,3), activation='relu'),
	MaxPooling2D(pool_size=(2,2)),
	Dropout(0.25),

	Conv2D(filters=64, kernel_size=(3,3), activation='relu'),
	MaxPooling2D(pool_size=(2,2)),
	Dropout(0.25),

	Flatten(),
	Dense(128, activation='relu'),
	Dropout(0.25),
	Dense(10, activation='softmax')
])

model.compile(optimizer='adam',
							loss=tf.keras.losses.SparseCategoricalCrossentropy(),
							metrics=[tf.keras.metrics.SparseCategoricalAccuracy()])

history = model.fit(training_set, validation_data=validation_set, epochs=50, callbacks=callbacks)

metrics_df = pd.DataFrame(history.history)
metrics_df[["loss", "val_loss"]].plot()
plt.show()
metrics_df[["sparse_categorical_accuracy", "val_sparse_categorical_accuracy"]].plot()
plt.show()

model.save('model')
loss, accuracy = model.evaluate(validation_set)
print('Accuracy on test dataset:', accuracy)
