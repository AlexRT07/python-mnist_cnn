import tensorflow as tf
from tensorflow import keras
import numpy as np
from tensorflow.keras.preprocessing import image

test_image = image.load_img('example2.png', color_mode="grayscale", target_size=(28,28))
test_image = image.img_to_array(test_image)

test_image = np.expand_dims(test_image, axis=0)

model = tf.keras.models.load_model('model')
prediction = model.predict(test_image)
print('Value pred:', prediction)
